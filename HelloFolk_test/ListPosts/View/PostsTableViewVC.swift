//
//  PostsTableViewVC.swift
//  Test_tessfold
//
//  Created by Rasha Alsaraiji on 10/08/2021.
//

import UIKit
import RxSwift
import RxCocoa


class PostsTableViewVC: UIViewController, UIScrollViewDelegate {
    
    var postsViewModel = PostsViewModel()
    @IBOutlet private weak var postsTableView: UITableView!
    
    
    public var posts = PublishSubject<[Post]>()
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
      
        postsViewModel.requestUsers()
        setupBinding()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    private func setupBinding(){
        
        postsTableView.register(UINib(nibName: "PostsTableViewCell", bundle: nil), forCellReuseIdentifier: String(describing: PostsTableViewCell.self))
        
        posts.bind(to: postsTableView.rx.items(cellIdentifier: "PostsTableViewCell", cellType: PostsTableViewCell.self)) {  (row,post,cell) in
            let user = self.postsViewModel.GetUserByIDD(id : post.user_id)
            cell.cellPost = post
            cell.cellUser = user
            }.disposed(by: disposeBag)
 
        
        postsTableView.rx.willDisplayCell
            .subscribe(onNext: ({ (cell,indexPath) in

            })).disposed(by: disposeBag)
        
        
      
        postsTableView.rx.modelSelected(Post.self).subscribe(onNext: { item in
            print("SelectedItem: \(item.title)")
            let user = self.postsViewModel.GetUserByIDD(id : item.user_id)
            let homeStoryboard = UIStoryboard(name: "Home", bundle:Bundle.main)
            let DetailPostsVC = homeStoryboard.instantiateViewController(withIdentifier: "DetailPostsVC") as! CommentsTableViewVC
            DetailPostsVC.PostSelected = item
            DetailPostsVC.UserSelected = user
            self.navigationController?.pushViewController(DetailPostsVC, animated: true)

        }).disposed(by: disposeBag)
    }
}






