//
//  HomeVC.swift
//  Test_tessfold
//
//  Created by Rasha Alsaraiji on 10/08/2021.
//

import UIKit
import RxSwift
import RxCocoa


class HomeVC: UIViewController {
    
    // MARK: - SubViews
    @IBOutlet weak var PostsVCView: UIView!
    

    private lazy var postsViewController: PostsTableViewVC = {
        // Load Storyboard
       
        let storyboard = UIStoryboard(name: "Home", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "PostsTableViewVC") as! PostsTableViewVC
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController, to: PostsVCView)
        
        return viewController
    }()
    
    
    var postsViewModel = PostsViewModel()
    
    let disposeBag = DisposeBag()
  
    // MARK: - View's Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        setupBindings()
        postsViewModel.requestData()
       
        
    }
   
    
    // MARK: - Bindings
    
    private func setupBindings() {
        
        // binding loading to vc
        
        postsViewModel.loading
            .bind(to: self.rx.isAnimating).disposed(by: disposeBag)
        
        // observing errors to show
        
        postsViewModel
            .error
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { (error) in
                switch error {
                case .internetError(let message):
                    MessageView.sharedInstance.showOnView(message: message, theme: .error)
                case .serverMessage(let message):
                    MessageView.sharedInstance.showOnView(message: message, theme: .warning)
                }
            })
            .disposed(by: disposeBag)
        
      
       
        postsViewModel
            .posts
            .observe(on: MainScheduler.instance)
            .bind(to: postsViewController.posts)
            .disposed(by: disposeBag)
        
            
       
    }
}


