//
//  SplashLaunchVC2.swift
//  Test_tessfold
//
//  Created by Rasha Alsaraiji on 12/08/2021.
//

import UIKit


class SplashLaunchVC2: UIViewController {
    
    @IBAction func NextSplash(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "Home", bundle: Bundle.main)
        let Splash2VC = storyboard.instantiateViewController(withIdentifier: "Splash2") as! SplashLaunchVC2
        Splash2VC.modalPresentationStyle = .fullScreen
        self.show(Splash2VC, sender: self)
    }
    @IBAction func BackSplash(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
}

