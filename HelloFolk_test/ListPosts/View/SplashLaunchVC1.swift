//
//  SplashLaunch1.swift
//  Test_tessfold
//
//  Created by Rasha Alsaraiji on 12/08/2021.
//

import UIKit
import SwiftyGif



class SplashLaunchVC1: UIViewController{
    @IBOutlet weak var NextSplash: UIButton!
    let logoAnimationView = LogoAnimationView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.navigationController?.setNavigationBarHidden(true, animated: true)
        //for launchScreen
        view.addSubview(logoAnimationView)
        logoAnimationView.pinEdgesToSuperView()
        logoAnimationView.logoGifImageView.delegate = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        logoAnimationView.logoGifImageView.startAnimatingGif()
    }
    
    @IBAction func NextSplash(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "Home", bundle: Bundle.main)
        let Splash2VC = storyboard.instantiateViewController(withIdentifier: "Splash2") as! SplashLaunchVC2
        Splash2VC.modalPresentationStyle = .fullScreen
        self.show(Splash2VC, sender: self)
    }
    
    
}

extension SplashLaunchVC1: SwiftyGifDelegate {
    func gifDidStop(sender: UIImageView) {
        logoAnimationView.isHidden = true
    }
}
