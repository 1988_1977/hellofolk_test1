//
//  CommentsTableViewVC.swift
//  Test_tessfold
//
//  Created by Rasha Alsaraiji on 14/08/2021.
//

import UIKit
import RxSwift
import RxCocoa


class CommentsTableViewVC: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet private weak var commentsTableView: UITableView!
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var postUser: UILabel!
    @IBOutlet weak var postBody: UITextView!
    
    var commentsViewModel = CommentsViewModel()
    public var PostSelected : Post = Post()!
    public var UserSelected : User = User()
  
    public var comments = PublishSubject<[Comment]>()
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        commentsViewModel.loading
            .bind(to: self.rx.isAnimating).disposed(by: disposeBag)
        
        // observing errors to show
        
        commentsViewModel
            .error
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { (error) in
                switch error {
                case .internetError(let message):
                    MessageView.sharedInstance.showOnView(message: message, theme: .error)
                case .serverMessage(let message):
                    MessageView.sharedInstance.showOnView(message: message, theme: .warning)
                }
            })
            .disposed(by: disposeBag)
        
       commentsViewModel
            .comments
            .observe(on: MainScheduler.instance)
        .bind(to: self.comments)
            .disposed(by: disposeBag)
        
        commentsViewModel.requestData(PostId: PostSelected.id)
        postTitle.text = PostSelected.title
        postBody.text = PostSelected.body
        postUser.text = UserSelected.name
        setupBinding()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    private func setupBinding(){
        
        commentsTableView.register(UINib(nibName: "CommentsTableViewCell", bundle: nil), forCellReuseIdentifier: String(describing: CommentsTableViewCell.self))
    
        comments.bind(to: commentsTableView.rx.items(cellIdentifier: "CommentsTableViewCell", cellType: CommentsTableViewCell.self)) {  (row,comment,cell) in
            cell.cellcomment = comment

            }.disposed(by: disposeBag)
       
         
      
        commentsTableView.rx.willDisplayCell
            .subscribe(onNext: ({ (cell,indexPath) in

            })).disposed(by: disposeBag)
        
        
    }
}






