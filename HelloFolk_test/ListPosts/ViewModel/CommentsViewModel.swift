//
//  CommentsViewModel.swift
//  Test_tessfold
//
//  Created by Rasha Alsaraiji on 14/08/2021.
//

import Foundation
import RxSwift
import RxCocoa



class CommentsViewModel {
    
    public enum CommmentsError {
        case internetError(String)
        case serverMessage(String)
    }
   
    //RxSwift Var
    public let comments : PublishSubject<[Comment]> = PublishSubject()
    public var users  = [User]()
    public let loading: PublishSubject<Bool> = PublishSubject()
   
    public let error : PublishSubject<CommmentsError> = PublishSubject()
    
    private let disposable = DisposeBag()

   
    public func requestData(PostId: Int){
       
        self.loading.onNext(true)
        APIManager.requestData(url: "comments", method: .get,parameters: nil, completion: { (result) in
            self.loading.onNext(false)
            switch result {
            case .success(let returnJson) :
                let comment = returnJson.arrayValue.compactMap {return Comment(data: try! $0.rawData())}
                let AcceptedComments = self.GetCommentsByPostId(id: PostId, AllComments: comment)
                self.comments.onNext(AcceptedComments)
            case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.error.onNext(.internetError("Check your Internet connection."))
                case .authorizationError(let errorJson):
                    self.error.onNext(.serverMessage(errorJson["message"].stringValue))
                default:
                    self.error.onNext(.serverMessage("Unknown Error"))
                }
            }
        })
        
    }
    public func GetCommentsByPostId (id :Int ,AllComments :[Comment])  -> [Comment]
     {
     var comments_res : [Comment] = [Comment]()
        for i in 0...AllComments.count-1
            {
            
            let temp  = AllComments[i]
          
           
            if (id ==  temp.postId)
            {
                comments_res.append(temp)
            }
            print("this issssssss",temp)
        }
        
       return comments_res
     }
}
