//
//  PostsViewModel.swift
//  Test_tessfold
//
//  Created by Rasha Alsaraiji on 10/08/2021.
//

import Foundation
import RxSwift
import RxCocoa



class PostsViewModel {
    
    public enum PostsError {
        case internetError(String)
        case serverMessage(String)
    }
    public enum UsersError {
        case internetError(String)
        case serverMessage(String)
    }
    //RxSwift Var
    public let posts : PublishSubject<[Post]> = PublishSubject()
    public var users  = [User]()
    public let loading: PublishSubject<Bool> = PublishSubject()  
    public let error : PublishSubject<PostsError> = PublishSubject()
    
    private let disposable = DisposeBag()
    
   public func GetUserByIDD (id :Int)  -> User
    {
    var user : User = User()
    if (self.users.count == 0)
    {return user }
     
    for i in 0...self.users.count-1
        {
            if (id == users[i].id)
            {
                user = users[i]
                return user
            }
        }
       return user
    }
    public func requestData(){
        
        self.loading.onNext(true)
        APIManager.requestData(url: "posts", method: .get, parameters: nil, completion: { (result) in
            self.loading.onNext(false)
            switch result {
            case .success(let returnJson) :
                
                let post = returnJson.arrayValue.compactMap {return Post(data: try! $0.rawData())}
                
                self.posts.onNext(post)
            case .failure(let failure) :
                switch failure {
                case .connectionError:
                    self.error.onNext(.internetError("Check your Internet connection."))
                case .authorizationError(let errorJson):
                    self.error.onNext(.serverMessage(errorJson["message"].stringValue))
                default:
                    self.error.onNext(.serverMessage("Unknown Error"))
                }
            }
        })
        
    }
    public func requestUsers(){
        
       
        APIManager.requestData(url: "users", method: .get, parameters: nil, completion: { (result) in
           
            switch result {
            case .success(let returnJson) :
                
                let user = returnJson.arrayValue.compactMap {return User(data: try! $0.rawData())}
                
                self.users = user
            case .failure(let failure) :
                switch failure {
                case .connectionError:
                    MessageView.sharedInstance.showOnView(message:"Check your Internet connection.", theme: .error)
                case .authorizationError(let errorJson):
                    MessageView.sharedInstance.showOnView(message :errorJson[""].stringValue, theme: .error)
                default:
                    MessageView.sharedInstance.showOnView(message :"Unknown Error", theme: .error)
                }
            }
        })
        
        
    }
}
