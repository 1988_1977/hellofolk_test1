//
//  Company.swift
//  Test_tessfold
//
//  Created by Rasha Alsaraiji on 10/08/2021.
//

import Foundation
struct Company: Codable {
    let  name,catch_Phrase,bs: String
   
    
    enum CodingKeys: String, CodingKey {
       
        case name = "name"
        case catch_Phrase = "catchPhrase"
        case bs = "bs"
        
    }
}
extension Company {
    init?() {
        name = ""
        catch_Phrase = ""
        bs = ""
        
    }
    init?(data: Data) {
        do {
            let me = try JSONDecoder().decode(Company.self, from: data)
            self = me
        }
        catch {
            print(error)
            return nil
        }
    }
}


