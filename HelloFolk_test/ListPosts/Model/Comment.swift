//
//  Comment.swift
//  Test_tessfold
//
//  Created by Rasha Alsaraiji on 10/08/2021.
//
 
import Foundation
struct Comment: Codable {
    let  name,email,body: String
    let postId , id :Int
   
    
    enum CodingKeys: String, CodingKey {
       
        case name = "name"
        case email = "email"
        case body = "body"
        case postId = "postId"
        case id = "id"
        
    }
    init?(data: Data) {
        do {
            let me = try JSONDecoder().decode(Comment.self, from: data)
            self = me
        }
        catch {
            print(error)
            return nil
        }
    }
}
