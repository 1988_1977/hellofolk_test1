//
//  Post.swift
//  Test_tessfold
//
//  Created by Rasha Alsaraiji on 10/08/2021.
//

import Foundation
struct Post: Codable {
    let id, user_id: Int
    let title: String
    let body :String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case user_id = "userId"
        case title = "title"
        case body = "body"
      
    }
}
extension Post {
    init?(data: Data) {
        do {
            let me = try JSONDecoder().decode(Post.self, from: data)
            self = me
        }
        catch {
            print(error)
            return nil
        }
    }
    init?() {
        id = 0
        user_id = 0
        title = ""
        body = ""
        
    }
    
}
