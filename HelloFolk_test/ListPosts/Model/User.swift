//
//  User.swift
//  Test_tessfold
//
//  Created by Rasha Alsaraiji on 10/08/2021.
//

 
import Foundation
struct User: Codable {
    let id :Int
    let name, username,email,phone,website: String
    let company :Company
    let address : Address
   
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case username = "username"
        case email = "email"
        case phone = "phone"
        case website = "website"
        case company = "company"
        case address = "address"
    }
}
extension User {
    init() {
        id = 0
        name = ""
        username = ""
        email = ""
        phone = ""
        website = ""
        company = Company()!
        address = Address()!
    }
    init?(data: Data) {
        do {
            let me = try JSONDecoder().decode(User.self, from: data)
            self = me
        }
        catch {
            print(error)
            return nil
        }
    }
}

