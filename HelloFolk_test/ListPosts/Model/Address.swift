//
//  Address.swift
//  Test_tessfold
//
//  Created by Rasha Alsaraiji on 10/08/2021.
//

import Foundation
struct Address: Codable {
    let  street,suite,city,zipcode: String
    let geo :Geo
   
    
    enum CodingKeys: String, CodingKey {
       
        case street = "street"
        case suite = "suite"
        case city = "city"
        case zipcode = "zipcode"
        case geo = "geo"
        
    }
}
extension Address {
    init?(){
        zipcode = ""
        street = ""
        suite = ""
        city = ""
        geo = Geo()!
    }
    init?(data: Data) {
        do {
            let me = try JSONDecoder().decode(Address.self, from: data)
            self = me
        }
        catch {
            print(error)
            return nil
        }
    }
}
