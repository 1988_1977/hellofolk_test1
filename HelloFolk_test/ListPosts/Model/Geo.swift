//
//  Geo.swift
//  Test_tessfold
//
//  Created by Rasha Alsaraiji on 10/08/2021.
//

import Foundation

struct Geo: Codable {
    let  lat , lng: String
   
    
    enum CodingKeys: String, CodingKey {
       
        case lat = "lat"
        case lng = "lng"
      
        
    }
}
extension Geo{
    init?(){
        lat = ""
        lng = ""
    }
    init?(data: Data) {
        do {
            let me = try JSONDecoder().decode(Geo.self, from: data)
            self = me
        }
        catch {
            print(error)
            return nil
        }
    }
}
