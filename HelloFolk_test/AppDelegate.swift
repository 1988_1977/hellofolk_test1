//
//  AppDelegate.swift
//  Test_tessfold
//
//  Created by Rasha Alsaraiji on 10/08/2021.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        let homeStoryboard = UIStoryboard(name: "Home", bundle: nil)
        
        let homeVC = homeStoryboard.instantiateViewController(withIdentifier: "Navigation")
      
        window?.rootViewController = homeVC
        window?.makeKeyAndVisible()
        // Override point for customization after application launch.
        return true
    }
  
   
}


