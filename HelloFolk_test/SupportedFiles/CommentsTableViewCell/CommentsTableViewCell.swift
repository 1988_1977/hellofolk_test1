//
//  CommentsTableViewCell.swift
//  Test_tessfold
//
//  Created by Rasha Alsaraiji on 14/08/2021.
//

import UIKit


class CommentsTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var commentTitle: UILabel!
  
    @IBOutlet weak var commentBody: UITextView!
   
    
    public var cellcomment : Comment! {
        didSet {
           
            self.commentTitle.text = cellcomment.name
            self.commentBody.text = cellcomment.body
          
        }
    }
    
    }

