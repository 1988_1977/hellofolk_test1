

import UIKit


class PostsTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var postUser: UILabel!
    @IBOutlet weak var postBody: UITextView!
   
    
    public var cellPost : Post! {
        didSet {
           
            self.postBody.text = cellPost.body
            self.postTitle.text = cellPost.title
          
        }
    }
    public var cellUser : User!{
        didSet {
           
            if ((cellUser) != nil)
           {
            self.postUser.text = cellUser.name
           }
           
          
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
    }
    
}
